use ::ast as ast;

mod lexer;
mod token;
mod interner;

use std::collections::HashMap;
use std::hash::{Hash, Hasher, SipHasher};
use parse::lexer::Lexer as Lexer;
use parse::token::Token as Token;
pub use parse::interner::Interner as Interner;
pub use parse::interner::InternerCtx as InternerCtx;

pub use parse::token::TokenReader as TokenReader;

pub struct Parser<'src> {
    curr: Option<Token<'src>>,
    cur_span: Option<ast::Span>,
    lexer: Lexer<'src>,
    curr_id: u64,
    src: &'src str,
    interner: InternerCtx<'src>
}

impl<'src> Parser<'src> {
    pub fn finalize(self) -> InternerCtx<'src> {
        self.interner
    }
    
    pub fn new_from_str(src: &'src str) -> Self {
        let mut lexer = Lexer::new(src);
        let (initial, span) = match lexer.next() {
            Some(ts) => {
                (Some(ts.0), Some(ts.1))
            },
            None => {
                (None, None)
            }
        };
        
        let interner = InternerCtx {
            interned_str_map: HashMap::new(),
            interned_name_map: HashMap::new()
        };
        
        let mut parser = Parser {
            interner: interner,
            curr: initial,
            cur_span: span,
            curr_id: 0,
            src: src,
            lexer: lexer
        };
        
        parser
    }
    
    pub fn span_to_str(&self, span: ast::Span) -> &'src str {
        let (start, finish, _) = span;
        &self.src[start..finish]   
    }
    
    fn advance(&mut self) {
        let (tok, span) = match self.lexer.next() {
            Some(ts) => {
                (Some(ts.0), Some(ts.1))
            },
            None => {
                (None, None)
            }
        };
        
        self.curr = tok;
        self.cur_span = span;
    }
    
    fn next_node_id(&mut self) -> ast::NodeId {
        self.curr_id += 1;
        
        ast::NodeId(self.curr_id)
    }
    
    fn expect(&mut self, token: Token<'src>) -> Result<Token<'src>, String> {
        let start_span = self.cur_span.unwrap_or((0, 0, 0));
        match self.curr {
            Some(tok) => {
                if tok == token {
                    self.advance();
                    Ok(tok)
                } else {
                    let end_span = self.cur_span.unwrap_or((0, 0, 0));
                    
                    Err(format!("{}:{} {}:{} error: expected {:?}, found {:?}\r\n{}\t {}", 
                            start_span.2, start_span.0,
                            end_span.2, end_span.0,
                            token,
                            tok,
                            start_span.2,
                            self.span_to_str(
                                (start_span.0, end_span.1, end_span.2)
                            ),
                        )
                    )
                }        
            },
            _ => {
                let end_span = self.cur_span.unwrap_or((0, 0, 0));
                
                Err(format!("{}:{} {}:{} error: expected {:?}, found <EOF>\r\n{}\t {}", 
                        start_span.2, start_span.0,
                        end_span.2, end_span.0,
                        token,
                        start_span.2,
                        self.span_to_str(
                            (start_span.0, end_span.1, end_span.2)
                        ),
                    )
                )
            }
        }
    }
    
    fn expect_ident(&mut self) -> Result<ast::Name, String> {
        let start_span = self.cur_span.unwrap_or((0, 0, 0));
         
        if let Some(Token::Ident(ident)) = self.curr {
            self.advance();
            Ok(self.interner.get_interned_str(ident))   
        } else {
            let end_span = self.cur_span.unwrap_or((0, 0, 0));
       
            match self.curr {
                Some(Token::Ident(ident)) => {
                    self.advance();
                    Ok(self.interner.get_interned_str(ident))
                },
                _ => {
                    let end_span = self.cur_span.unwrap_or((0, 0, 0));
                    
                    Err(format!("{}:{} {}:{} error: expected an identifier, found {:?}\r\n{}\t {}", 
                            start_span.2, start_span.0,
                            end_span.2, end_span.0,
                            self.curr.unwrap(), 
                            start_span.2,
                            self.span_to_str(
                                (start_span.0, end_span.1, end_span.2)
                            ),
                        )
                    )           
                }
            }
        }
    }
}

impl<'src> TokenReader for Parser<'src> {
    fn parse_expr(&mut self) -> Result<ast::Expr, String> {
        if let Some(c) = self.curr {
            let expr_kind = match c {
                Token::Number(num) => { self.advance(); ast::ExprKind::Number(num)},
                Token::StrLit(lit) => { self.advance(); ast::ExprKind::StrLit(self.interner.get_interned_str(lit))},
                Token::Ident(ident) => {
                    let ident = try!(self.expect_ident());
                    
                    if let Some(Token::LParen) = self.curr {
                        try!(self.expect(Token::LParen));
                        try!(self.expect(Token::RParen));
                        
                        ast::ExprKind::Call(ident, vec![])              
                    } else {
                        ast::ExprKind::Value(ident)
                    }
                },
                Token::Bool(tf) => { self.advance(); ast::ExprKind::Bool(tf) },
                Token::LParen => {
                    try!(self.expect(Token::LParen));
                    try!(self.expect(Token::RParen));
                    
                    ast::ExprKind::Unit
                },
                _ => unimplemented!()
            };
            
            Ok(ast::Expr::new(
                self.next_node_id(),
                expr_kind
            ))
        } else {
            unimplemented!()
        }
    }

    // <ident> = <expr>; 
    fn parse_assignment_stmt(&mut self, ident: ast::Name) -> Result<ast::Statement, String> {
        try!(self.expect(Token::Eq));  
        let expr = try!(self.parse_expr());
        
        let stmt = ast::Statement::new_assign(
            self.next_node_id(),
            ident,
            expr
        );
        
        Ok(stmt)   
    }
    
    fn parse_ident_stmt(&mut self) -> Result<ast::Statement, String> {
        let ident = try!(self.expect_ident());
        
        let stmt = match self.curr {
            // <ident> = <expr>               
            Some(Token::Eq) => try!(self.parse_assignment_stmt(ident)),
            _ => {
                let expr = try!(self.parse_expr());
                
                ast::Statement::new_expr(
                    self.next_node_id(),
                    expr
                )
            }
        };
        
        try!(self.expect(Token::Semi));
        
        Ok(stmt)
    }
    
    // if <expr> <block> else <block>
    fn parse_if_stmt(&mut self) -> Result<ast::Statement, String> {
        try!(self.expect(Token::If));
        let expr = try!(self.parse_expr());
        let block = try!(self.parse_block());
        
        // There should be an else clause getting parsed to.
        let stmt = ast::Statement::new_if(self.next_node_id(), expr, block);
        
        Ok(stmt)
    }
    
    // let <ident> = <expr>; 
    fn parse_let_stmt(&mut self) -> Result<ast::Statement, String> {
        try!(self.expect(Token::Let)); 
        let ident = try!(self.expect_ident());
        try!(self.expect(Token::Eq));
        let expr = try!(self.parse_expr());
        
        try!(self.expect(Token::Semi));
        
        let stmt = ast::Statement::new_decl(
            self.next_node_id(),
            ast::Decl::new_local(ast::Local::new(
                self.next_node_id(),
                ident,
                expr
            ))
        );
        
        Ok(stmt)
    }
    
    // parses statements within blocks
    fn parse_stmt(&mut self) -> Result<ast::Statement, String> {
        let stmt = match self.curr.unwrap() {
            Token::Let => try!(self.parse_let_stmt()),
            Token::LBrace => { 
                let block = ast::Statement::new_block(self.next_node_id(), try!(self.parse_block()));
                try!(self.expect(Token::Semi));
                
                block
            },
            Token::If => try!(self.parse_if_stmt()),
            Token::Ident(ident) => try!(self.parse_ident_stmt()),
            _ => panic!("NYI {:?}", self.curr)
        };
        
        Ok(stmt)
    }
    
    // parses function blocks and anonymous inner blocks
    fn parse_block(&mut self) -> Result<ast::Block, String> {
        let mut block = ast::Block::new(self.next_node_id());        
        try!(self.expect(Token::LBrace));

        while let Some(tok) = self.curr {
            if let Some(Token::RBrace) = self.curr {
                break;   
            }
            
            let stmt = try!(self.parse_stmt());
            block.insert_statement(stmt);
        }
          
        try!(self.expect(Token::RBrace));
        Ok(block)
    }
    
    fn parse_fn_sig(&mut self) -> Result<ast::Name, String> {
        let start_span = self.cur_span.unwrap_or((0, 0, 0));
        
        try!(self.expect(Token::Fn));
        let fn_name = try!(self.expect_ident());
        
        try!(self.expect(Token::LParen));
        try!(self.expect(Token::RParen));

        Ok(fn_name)
    }
    
    fn parse_fn(&mut self) -> Result<ast::Item, String> {
        let fn_name = try!(self.parse_fn_sig());
        let block = try!(self.parse_block());
        
        let item = ast::Item::new(
                        self.next_node_id(),
                        fn_name,
                        ast::ItemKind::new_fn(ast::FnDecl::new(), block)
                    );
        
        Ok(item)
    }
    
    fn parse_module_sig(&mut self) -> Result<ast::Name, String> {
        try!(self.expect(Token::Mod));
        let module_name = self.expect_ident();
        
        module_name
    }
    
    fn parse_module_item(&mut self) -> Result<ast::Item, String> {
        let module_name = try!(self.parse_module_sig());
                    
        try!(self.expect(Token::LBrace));
        let module = try!(self.parse_module());
        try!(self.expect(Token::RBrace));
                    
        let item = ast::Item::new(
                        self.next_node_id(),
                        module_name,
                        ast::ItemKind::new_module(module));
        
        Ok(item)
    }
    
    fn parse_module(&mut self) -> Result<ast::Module, String> {        
        let mut items = vec![];
 
        while let Some(curr) = self.curr {
            let item = match curr {
                Token::Mod => try!(self.parse_module_item()),
                Token::Fn => try!(self.parse_fn()),
                _ => {
                    unimplemented!();
                }
            };
            
            items.push(item);   
        }
        
        Ok(ast::Module::new(self.next_node_id(), items))
    }
    
    fn parse_krate(&mut self) -> Result<ast::Krate, String> { 
        let name = self.interner.get_interned_str("hello_world");
        let module = try!(self.parse_module());
        
        Ok(ast::Krate::new(
            name, 
            module
        ))
    }
}
