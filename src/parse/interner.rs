use std::hash::{Hash, Hasher, SipHasher};
use std::collections::HashMap;
use ::parse::Parser;
use ::ast as ast;

#[derive(Debug, Clone)]
pub struct InternerCtx<'src> {
    pub interned_str_map: HashMap<&'src str, ast::Name>,
    pub interned_name_map: HashMap<ast::Name, &'src str>
}

pub trait Interner<'src> {
    fn get_interned_str(&mut self, name: &'src str) -> ast::Name;
    fn get_interned_name(&mut self, name: &ast::Name) -> &'src str;
    fn insert_interned_str(&mut self, name: &'src str) -> ast::Name;
}

impl<'src> Interner<'src> for InternerCtx<'src> {
    fn get_interned_str(&mut self, name: &'src str) -> ast::Name {        
        match self.interned_str_map.get(name) {
            Some(s) => return *s,
            None => {}
        };
        
        self.insert_interned_str(name)
    }
    
    fn get_interned_name(&mut self, name: &ast::Name) -> &'src str {        
        match self.interned_name_map.get(name) {
            Some(s) => return s,
            None => unimplemented!()
        };
    }
    
    fn insert_interned_str(&mut self, value: &'src str) -> ast::Name {
        let atom = ast::hash(&value);
        self.interned_str_map.insert(value, ast::Name(atom));
        self.interned_name_map.insert(ast::Name(atom), value);
        
        ast::Name(atom)
    }
}