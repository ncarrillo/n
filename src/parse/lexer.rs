use parse::token::Token as Token;
use ast;

pub struct Lexer<'src> {
    src: &'src str,
    curr: Option<char>,
    line: usize,
    pos: usize
}

impl<'src> Lexer<'src> {
    pub fn new(src: &'src str) -> Self {
        let mut lexer = Lexer {
            src: src,
            line: 0,
            pos: 0,
            curr: src[0..].chars().next()
        };
        
        lexer
    }
    
    fn advance(&mut self) {
        self.pos += 1;
        self.curr = self.src[self.pos..].chars().next();
    }
    
    fn skip_whitespace(&mut self) {
        while let Some(c) = self.curr {
            if !c.is_whitespace() {
                break;    
            }
            
            if c == '\n' {
                self.line += 1;    
            }
            
            // Need to parse newlines more intelligently for comments
            self.advance();
        }
    }
    
    fn reserved_word(&mut self, ident: &'src str) -> Option<Token<'src>> {
        match ident {
            "fn" => Some(Token::Fn),
            "return" => Some(Token::Return),
            "if" => Some(Token::If),
            "mod" => Some(Token::Mod),
            "let" => Some(Token::Let),
            "struct" => Some(Token::Struct),
            "enum" => Some(Token::Enum),
            "true" => Some(Token::Bool(true)),
            "false" => Some(Token::Bool(false)),
            _ => None
        }    
    }
    
    fn next_token(&mut self) -> Option<(Token<'src>, ast::Span)> {
        self.skip_whitespace();
        
        if let None = self.curr {
            return None;
        }
        
        let s = self.pos;
        
        let c = self.curr.unwrap();
        let token = match c {
           '{' => { 
                self.advance();
                Token::LBrace
           },
           '}' => {
                self.advance();
                Token::RBrace
           },
           '(' => {
                self.advance();
                Token::LParen
           },
           ')' => {
                self.advance();
                Token::RParen
           },
           '!' => {
                self.advance();
                Token::Not
           },
           '+' => {
                self.advance();
                Token::Add
           },
           '-' => {
                self.advance();
                Token::Sub
           },
           '*' => {
                self.advance();
                Token::Mul
           },
           '/' => {
                self.advance();
                Token::Div
           },
           ';' => {
                self.advance();
                Token::Semi
           },
           ',' => {
               self.advance();
               Token::Comma
           }
           ':' => {
                self.advance();
                Token::Colon
           },
           '"' => {
                self.advance();
                let start = self.pos;
                
                while let Some(c) = self.curr {
                    if c == '"' {
                        self.advance();
                        break;
                    } else {
                        self.skip_whitespace();
                        self.advance();
                    }
                }
                
                let ident = &self.src[start .. self.pos];
                Token::StrLit(ident)
           }
           '=' => {
                self.advance();
                Token::Eq
           },
           '\n' => {
               panic!("NEW LINE");
           }
           c if c.is_alphabetic() || c == '_' => {
                let start = self.pos;
                
                while let Some(c) = self.curr {
                    if !c.is_alphabetic() && !c.is_numeric() && c != '_' {
                        break;
                    } else {
                        self.skip_whitespace();
                        self.advance();
                    }
                }
                
                let ident = &self.src[start .. self.pos];
                self.reserved_word(ident).unwrap_or(Token::Ident(ident))
           },
           c if c.is_numeric() => {
                let start = self.pos;
                
                while let Some(c) = self.curr {
                    if !c.is_numeric() {
                        break;
                    } else {
                        self.skip_whitespace();
                        self.advance();
                    }
                }
                
                let ident = &self.src[start .. self.pos];
                Token::Number(ident.parse().unwrap_or(0))          
           }
           _ => panic!("error: Unexpected character {}", c)
        };
        
        Some((token, (s, self.pos, self.line)))
    }
}

impl<'src> Iterator for Lexer<'src> {
    type Item = (Token<'src>, ast::Span);
    
    fn next(&mut self) -> Option<Self::Item> {
        self.next_token()
    }    
}