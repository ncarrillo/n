use std::fmt::{Debug};
use std::fmt;
use ::ast as ast;

#[derive(Copy, Clone, Eq, PartialEq)]
pub enum Token<'src> {
    Lt,
    Gt,
    Eq,
    EqEq,
    Not,
    LBrace,
    RBrace,
    LParen,
    RParen,
    Comma,
    Colon,
    Add,
    Sub,
    Number(usize),
    StrLit(&'src str),
    Bool(bool),
    Mul,
    Div,
    Semi,
    Let,
    Struct,
    Enum,
    Fn,
    Return,
    Mod,
    If,
    Ident(&'src str)
}

impl<'src> Debug for Token<'src> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &Token::Number(num) => write!(f, "{}", num),
            &Token::StrLit(lit) => write!(f, "{}", lit),
            &Token::Bool(tf) => write!(f, "{}", tf),
            &Token::Not => write!(f, "!"),
            &Token::Semi => write!(f, ";"),
            &Token::Comma => write!(f, ","),
            &Token::Colon => write!(f, ":"),
            &Token::LParen => write!(f, "("),
            &Token::RParen => write!(f, ")"),
            &Token::If => write!(f, "keyword 'if'"),
            &Token::Mod => write!(f, "keyword 'mod'"),
            &Token::Return => write!(f, "keyword 'return'"),
            &Token::Fn => write!(f, "keyword 'fn'"),
            &Token::Struct => write!(f, "keyword 'struct'"),
            &Token::Enum => write!(f, "keyword 'enum'"),
            &Token::Eq => write!(f, "'='"),
            &Token::EqEq => write!(f, "'=='"),
            &Token::Mul => write!(f, "'*'"),
            &Token::Div => write!(f, "'/'"),
            &Token::Add => write!(f, "'+'"),
            &Token::Sub => write!(f, "'-'"),
            &Token::LBrace => write!(f, "opening brace '{{'"),
            &Token::RBrace => write!(f, "closing brace '}}'"),
            _ => unimplemented!()
        }
    }
}

pub trait TokenReader {
    // Expression parsing
    fn parse_expr(&mut self) -> Result<ast::Expr, String>;
    
    // Statement parsing
    fn parse_assignment_stmt(&mut self, ident: ast::Name) -> Result<ast::Statement, String>;
    fn parse_ident_stmt(&mut self) -> Result<ast::Statement, String>;
    fn parse_if_stmt(&mut self) -> Result<ast::Statement, String>;
    fn parse_let_stmt(&mut self) -> Result<ast::Statement, String>;
    fn parse_stmt(&mut self) -> Result<ast::Statement, String>;
    
    // Fn parsing
    fn parse_block(&mut self) -> Result<ast::Block, String>;
    fn parse_fn_sig(&mut self) -> Result<ast::Name, String>;
    fn parse_fn(&mut self) -> Result<ast::Item, String>;
    
    // Module parsing 
    fn parse_module_sig(&mut self) -> Result<ast::Name, String>;
    fn parse_module_item(&mut self) -> Result<ast::Item, String>;
    fn parse_module(&mut self) -> Result<ast::Module, String>;
    
    // Krate parsing
    fn parse_krate(&mut self) -> Result<ast::Krate, String>;
}