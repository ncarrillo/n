#![feature(box_syntax, box_patterns, rustc_private, libc)]

extern crate rustc;
extern crate rustc_llvm;
extern crate libc;
use libc::*;
use std::ffi::CString;
use std::str;
mod ast;
mod mir;
mod parse;
mod typeck;

use std::path::PathBuf;
use parse::TokenReader;
use ast::Visitor;
use std::collections::HashMap;
use parse::Interner;

pub fn noname() -> *const c_char { 
     static CNULL: c_char = 0; 
     &CNULL 
} 

// We need to track the current basic block here.
#[derive(Debug)]
pub struct TransCtx {
    cur_bb: Option<rustc_llvm::BasicBlockRef>,
    cur_fn: Option<rustc_llvm::ValueRef>,
    cur_builder: Option<rustc_llvm::BuilderRef>
}

#[derive(Debug)]
pub struct TransPass<'src> {
    typeck_result: typeck::Context<'src>,
    ctx: rustc_llvm::ContextRef,
    module: rustc_llvm::ModuleRef,
    trans_ctx_stack: Vec<ast::NodeId>,
    trans_ctx: ast::NodeId,
    trans_ctx_map: HashMap<ast::NodeId, TransCtx>
}

impl<'src> TransPass<'src> {
    fn create_llvm_contexts() -> (rustc_llvm::ContextRef, rustc_llvm::ModuleRef) {
        unsafe {
            let ctx = rustc_llvm::LLVMContextCreate();
            let module = rustc_llvm::LLVMModuleCreateWithNameInContext(
                CString::new("Test").unwrap().as_ptr(), 
                ctx);
            
            (ctx, module)
        }
    }
    
    unsafe fn new(context: typeck::Context<'src>) -> Self {
        println!("{:#?}", context);
        let (ctx, module) = TransPass::create_llvm_contexts();
                    
        TransPass {
            typeck_result: context,
            ctx: ctx,
            module: module,
            trans_ctx_stack: vec![],
            trans_ctx_map: HashMap::new(),
            trans_ctx: ast::NodeId(0)
        }
    }
}

impl<'src> ast::Visitor for TransPass<'src> {
    type Output = rustc_llvm::ModuleRef;

    fn visit_item(&mut self, item: &ast::Item) {
        unsafe {
            if let &ast::ItemKind::Fn(box ref decl, box ref block) = item.kind() {
                let fn_ty = rustc_llvm::LLVMFunctionType(
                    rustc_llvm::LLVMVoidTypeInContext(self.ctx), 
                    rustc_llvm::LLVMVoidTypeInContext(self.ctx) as *const rustc_llvm::TypeRef, 0, 0);
                    
                let fn_obj = rustc_llvm::LLVMGetOrInsertFunction(
                    self.module, 
                    CString::new(self.typeck_result.interner_ctx.get_interned_name(&item.name())).unwrap().as_ptr(), 
                    fn_ty);
                    
                self.trans_ctx_stack.push(item.id());
                if let None = self.trans_ctx_map.get(&item.id()) {
                    self.trans_ctx_stack.push(item.id());
                    self.trans_ctx = item.id();

                    self.trans_ctx_map.insert(item.id(), 
                        TransCtx {
                            cur_fn: Some(fn_obj),
                            cur_bb: None,
                            cur_builder: None
                        }
                    );
                }
            }
        }
        
        ast::walk_item(self, item);
    }  
    
    fn visit_block(&mut self, block: &ast::Block) {
        unsafe {
            let mut trans_ctx = self.trans_ctx_map.get_mut(&self.trans_ctx).unwrap();
            let bb = rustc_llvm::LLVMAppendBasicBlockInContext(self.ctx, trans_ctx.cur_fn.unwrap(), CString::new("entry").unwrap().as_ptr()); 
            let builder = rustc_llvm::LLVMCreateBuilderInContext(self.ctx);
            
            trans_ctx.cur_bb = Some(bb);
            trans_ctx.cur_builder = Some(builder);
            rustc_llvm::LLVMPositionBuilderAtEnd(builder, bb);
        }
        
        self.typeck_result.push_scope(block.id());
        ast::walk_block(self, block);
        self.typeck_result.pop_scope();
    }
    
    fn visit_statement(&mut self, stmt: &ast::Statement) {   
        match stmt {
            &ast::Statement::Decl(box ref decl, node_id) => {
                match decl {
                    &ast::Decl::Local(box ref local) => {
                        unsafe {
                            let trans_ctx = self.trans_ctx_map.get(&self.trans_ctx).expect("trans_ctx lookup failed");
                            let local_ty = rustc_llvm::LLVMInt32TypeInContext(self.ctx);
                            let val = rustc_llvm::LLVMBuildAlloca(
                                trans_ctx.cur_builder.unwrap(), 
                                local_ty, 
                                CString::new(self.typeck_result.interner_ctx.get_interned_name(&local.name())).unwrap().as_ptr());
                        
                            rustc_llvm::LLVMBuildStore(
                                trans_ctx.cur_builder.unwrap(), 
                                rustc_llvm::LLVMConstInt(rustc_llvm::LLVMInt32TypeInContext(self.ctx), 10, 0),
                                val);
                        }
                    },
                    _ => unimplemented!()
                }
            },
            &ast::Statement::If(box ref expr, box ref block, node_id) => {
                self.visit_block(block);
            },
            
            &ast::Statement::Block(box ref block, node_id) => {
                self.visit_block(block);
            },
            _ => {}
        }
    }
    
    fn finalize(self) -> Self::Output {
        unsafe {
            rustc_llvm::LLVMDumpModule(self.module);
        }
        
        self.module
    }
}

fn main() {
    /*unsafe {    
        rustc_llvm::LLVMRustBuildCall(builder, fn_obj2, v.as_ptr(), 0 as c_uint, 0 as *mut _, noname());
        rustc_llvm::LLVMDumpModule(mdl);
    }*/
    
    let mut parser = parse::Parser::new_from_str(
        "
        fn main() { 
            let x = 0;
            let y = 10;
        }
        
        fn test() {
            let blah = 10;
            blah = 20;
        }
        ");
  
    let krate = match parser.parse_krate() {
        Ok(krate) => krate,
        Err(failure) => {
            println!("{}", failure);
            return;
        }
    };
    
    match typeck::run_typeck(parser.finalize(), &krate) {
        Ok(ctx) => {
            unsafe {
                let mut v = TransPass::new(ctx);
                ast::walk_krate(&mut v, &krate);
                
                let llvm_mod = v.finalize();
            } 
        },
        Err(failures) => {
            for f in failures {
                println!("error: mismatched types. expected type {:#?}, found type {:#?}", f.expected, f.got);                
            }
        }
    }

}