mod gather_pass;
mod infer_pass;
mod check_pass;
mod context;

use ast;
use ast::Visitor;
use std::collections::HashMap;

pub use self::gather_pass::*;
pub use self::infer_pass::*;
pub use self::check_pass::*;
pub use self::context::*;

pub type TypeMap = HashMap<ast::TyId, Ty>;
pub type NodeTypeMap = HashMap<ast::NodeId, ast::TyId>;

#[derive(Debug, Clone)]
pub enum Ty {
    Unit,
    i32,
    Str,
    Bool,
    Fn,
    Composite {
        fields: HashMap<ast::TyId, ast::NodeId>
    }
}

pub fn run_typeck<'a>(interner_ctx: ::parse::InternerCtx<'a>, krate: &ast::Krate) -> Result<Context<'a>, Vec<CheckFailure>> {
    // Need a way for a krate parsing session to return the interner tables
    let mut ctx = Context::new(interner_ctx);
    
    let mut ty_gp = GatherPass::new(ctx);
    ast::walk_krate(&mut ty_gp, &krate);
    ctx = ty_gp.finalize();
    
    let mut ty_ip = InferPass::new(ctx);
    ast::walk_krate(&mut ty_ip, &krate);
    ctx = ty_ip.finalize();
    
    let mut ty_cp = CheckPass::new(ctx);    
    ast::walk_krate(&mut ty_cp, &krate);
    
    ty_cp.finalize()
}