use ::ast as ast;

#[derive(Debug)]
pub struct GatherPass<'src> {
    ctx: ::typeck::Context<'src>
}

impl<'src> GatherPass<'src> {
    pub fn new(ctx: ::typeck::Context<'src>) -> Self {
        GatherPass {
            ctx: ctx
        }
    }
}

impl<'src> ast::Visitor for GatherPass<'src> {
    type Output = ::typeck::Context<'src>;
    
    fn finalize(self) -> Self::Output {
        self.ctx
    }
    
    fn visit_block(&mut self, block: &ast::Block) {
        self.ctx.push_scope(block.id());
        ast::walk_block(self, block);
        self.ctx.pop_scope();
    }
    
    fn visit_module(&mut self, module: &ast::Module) { 
        self.ctx.push_scope_no_parent(module.id());
        ast::walk_module(self, module);
        self.ctx.pop_scope();
    }
    
    fn visit_item(&mut self, item: &ast::Item) {
        if let &ast::ItemKind::Fn(box ref fn_decl, _) = item.kind() {
            self.ctx.insert_ident_node(item.name(), item.id());
            self.ctx.insert_ty(ast::TyId::from_fn(&fn_decl), ::typeck:: Ty::Fn);
            self.ctx.insert_node_ty(item.id(), ast::TyId::from_fn(&fn_decl));
        }
        
        ast::walk_item(self, item);
    }
    
    fn visit_statement(&mut self, stmt: &ast::Statement) {
        match stmt {
            &ast::Statement::Decl(box ref decl, node_id) => {
                match decl {
                    &ast::Decl::Local(box ref local) => {
                        self.ctx.insert_ident_node(local.name(), local.id());
                    },
                    _ => unimplemented!()
                }
            },
            &ast::Statement::If(box ref expr, box ref block, node_id) => {
                self.visit_block(block);
            },
            
            &ast::Statement::Block(box ref block, node_id) => {
                self.visit_block(block);
            },
            _ => {}
        }
    }
}