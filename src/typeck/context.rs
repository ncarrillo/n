use std::collections::{HashMap};
use ::ast as ast;
use ::typeck::Ty;

#[derive(Debug, Clone)]
pub struct Context<'src> {
    scope_map: HashMap<ast::NodeId, Scope>,
    pub cur_scope: ast::NodeId,
    pub scope_stack: Vec<ast::NodeId>,
    pub interner_ctx: ::parse::InternerCtx<'src>
}

impl<'src> Context<'src> {
    pub fn new(interner_ctx: ::parse::InternerCtx<'src>) -> Self {
        Context {
            scope_map: HashMap::new(),
            cur_scope: ast::NodeId(0),
            scope_stack: vec![],
            interner_ctx: interner_ctx
        }
    }
    
    pub fn push_scope(&mut self, block_id: ast::NodeId) {
        if let None = self.scope_map.get(&block_id) {
            self.scope_map.insert(block_id, Scope::new(Some(self.cur_scope)));    
        }
        
        self.cur_scope = block_id;
        self.scope_stack.push(block_id);
    }
    
    pub fn push_scope_no_parent(&mut self, block_id: ast::NodeId) {
        if let None = self.scope_map.get(&block_id) {
            self.scope_map.insert(block_id, Scope::new(None));    
        }
        
        self.cur_scope = block_id;
        self.scope_stack.push(block_id);
    }
    
    pub fn pop_scope(&mut self) {
        self.scope_stack.pop();
        
        if self.scope_stack.len() > 0 {
            self.cur_scope = self.scope_stack[self.scope_stack.len() - 1];
        }
    }
    
    pub fn insert_node_ty(&mut self, id: ast::NodeId, ty_id: ast::TyId) {
        let scope = self.scope_map.get_mut(&self.cur_scope).expect("insert_node_ty: scope lookup failed.");
        scope.node_map.insert(id, ty_id);
    }
    
    pub fn insert_ty(&mut self, id: ast::TyId, ty: Ty) {   
        let scope = self.scope_map.get_mut(&self.cur_scope).expect("insert_node_ty: scope lookup failed.");
        scope.ty_map.insert(id, ty); 
    }
        
    pub fn insert_ident_node(&mut self, name: ast::Name, id: ast::NodeId) {
        let scope = self.scope_map.get_mut(&self.cur_scope).expect("insert_ident_node: scope lookup failed.");
        scope.ident_map.insert(name, id);
    }
    
    pub fn resolve_node_ty(&self, id: ast::NodeId) -> Option<ast::TyId> {
        let mut scope = self.scope_map.get(&self.cur_scope).unwrap();
        
        if let Some(ident_id) = scope.resolve_node_ty(id) {
            return Some(ident_id);
        } 
        
        while let Some(parent_id) = scope.parent_id() {
            scope = self.scope_map.get(&parent_id).unwrap();
            
            if let Some(ident_id) = scope.resolve_node_ty(id) {
                return Some(ident_id);
            }
        }
        
        None        
    }
    
    pub fn resolve_ident(&self, name: ast::Name) -> Option<ast::NodeId> {
        let mut scope = self.scope_map.get(&self.cur_scope).unwrap();
        
        if let Some(ident_id) = scope.resolve_ident(name) {
            return Some(ident_id);
        } 
        
        while let Some(parent_id) = scope.parent_id() {
            scope = self.scope_map.get(&parent_id).unwrap();
            
            if let Some(ident_id) = scope.resolve_ident(name) {
                return Some(ident_id);
            }
        }
        
        None
    }
    
    pub fn resolve_ty(&self, id: ast::TyId) -> Option<Ty> {
        let mut scope = self.scope_map.get(&self.cur_scope).unwrap();
        
        if let Some(ty) = scope.resolve_ty(id) {
            return Some(ty);
        } 
        
        while let Some(parent_id) = scope.parent_id() {
            scope = self.scope_map.get(&parent_id).unwrap();
            
            if let Some(ty) = scope.resolve_ty(id) {
                return Some(ty);
            }
        }
        
        None
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub struct ScopeId(pub u32);

#[derive(Debug, Clone)]
pub struct Scope {
    pub parent: Option<ast::NodeId>,
    pub ident_map: HashMap<ast::Name, ast::NodeId>,
    pub node_map: HashMap<ast::NodeId, ast::TyId>,
    pub ty_map: HashMap<ast::TyId, Ty>
}

impl Scope {
    fn new(parent: Option<ast::NodeId>) -> Self {
        Scope {
            parent: parent,
            ident_map: HashMap::new(),
            node_map: HashMap::new(),
            ty_map: HashMap::new()
        }
    }
    
    pub fn parent_id(&self) -> Option<ast::NodeId> {
        self.parent
    }
}

pub trait Resolver {
    fn resolve_ident(&self, name: ast::Name) -> Option<ast::NodeId>;
    fn resolve_node_ty(&self, id: ast::NodeId) -> Option<ast::TyId>;
    fn resolve_ty(&self, id: ast::TyId) -> Option<Ty>;
}

impl Resolver for Scope {
    fn resolve_ident(&self, name: ast::Name) -> Option<ast::NodeId> {
        self.ident_map.get(&name).map(|n| *n)
    }
    
    fn resolve_node_ty(&self, id: ast::NodeId) -> Option<ast::TyId> {
        self.node_map.get(&id).map(|tid| *tid)
    }
    
    fn resolve_ty(&self, id: ast::TyId) -> Option<Ty> {
        match id {
            ast::TyId(1) => Some(Ty::i32),
            ast::TyId(2) => Some(Ty::Str),
            ast::TyId(3) => Some(Ty::Bool),
            ast::TyId(4) => Some(Ty::Unit),
            _ => self.ty_map.get(&id).map(|ty| ty.clone())
        }
    }
}
