use ::ast as ast;
use ::typeck::Resolver;

#[derive(Debug)]
pub struct CheckPass<'src> {
    ctx: ::typeck::Context<'src>,
    failures: Vec<CheckFailure>
}

impl<'src> CheckPass<'src> {
    pub fn new(context: ::typeck::Context<'src>) -> Self {
        CheckPass {
            ctx: context,
            failures: vec![]
        }
    }
}

#[derive(Debug)]
pub struct CheckFailure {
    pub expected: ::typeck::Ty,
    pub got: ::typeck::Ty
}

impl<'src> ast::Visitor for CheckPass<'src> {
    type Output = Result<::typeck::Context<'src>, Vec<CheckFailure>>;
    
    fn finalize(self) -> Self::Output {
        if self.failures.len() > 0 {
            Err(self.failures)
        } else {
            Ok(self.ctx)  
        }  
    }
    
    fn visit_block(&mut self, block: &ast::Block) {
        self.ctx.push_scope(block.id());
        ast::walk_block(self, block);
        self.ctx.pop_scope();
    }
    
    fn visit_statement(&mut self, stmt: &ast::Statement) {
        match stmt {
            &ast::Statement::Decl(box ref decl, node_id) => {
                match decl {
                    &ast::Decl::Local(box ref local) => {
                        let binding_ty = self.ctx.resolve_node_ty(local.id()).expect("WOOT:");
                        let expr_ty = self.ctx.resolve_node_ty(local.expr().id()).expect("WEWT");
                        
                        // This gets a little more complex. We cant assume the expr ty == the fn ty 
                        // We either need to typeck the params of the fn call here, or in inference.
                        if binding_ty != expr_ty {
                            let expected_ty = self.ctx.resolve_ty(binding_ty).expect("WTF");
                            let got_ty = self.ctx.resolve_ty(expr_ty).expect("WAT");
                            
                            self.failures.push(CheckFailure {
                               expected: expected_ty,
                               got: got_ty 
                            });
                        }
                    },
                    _ => {}
                }        
            },
            &ast::Statement::Assign(name, box ref expr, node_id) => {
                // This almost should not be here..review..
                // Why doesn't this work exactly like Decls? Is it because gather doesn't walk assignments?
                let expr_ty_id = self.ctx.resolve_node_ty(expr.id()).expect("WEWT");
                let local_ty = self.ctx.resolve_node_ty(node_id).unwrap();
                     
                if local_ty != expr_ty_id {
                    let expected_ty = self.ctx.resolve_ty(local_ty).expect("wet");
                    let got_ty = self.ctx.resolve_ty(expr_ty_id).expect("wot");
                            
                    self.failures.push(CheckFailure {
                        expected: expected_ty,
                        got: got_ty 
                    });
                }
            },
            &ast::Statement::Block(box ref block, node_id) => {
                self.visit_block(block);
            },
            &ast::Statement::If(box ref expr, box ref block, node_id) => {
                let ty_id = self.ctx.resolve_node_ty(expr.id()).unwrap();

                if ty_id != ast::TyId(3) {
                    self.failures.push(CheckFailure {
                        expected: ::typeck::Ty::Bool,
                        got: self.ctx.resolve_ty(ty_id).unwrap() 
                    });
                }
                
                self.visit_block(block);  
            },
            &ast::Statement::Expr(ref expr, node_id) => {
                if let &ast::ExprKind::Call(fn_name, ref args) = expr.kind() {
                    if let Some(fn_id) = self.ctx.resolve_ident(fn_name) {
                        println!("Found fn id: {:#?}", fn_id);       
                    } else {
                        panic!("undefined fn");
                    }
                }
            }
        }
    }    
}