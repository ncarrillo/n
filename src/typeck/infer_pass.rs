use ::ast as ast;

#[derive(Debug)]
pub struct InferPass<'src> {
    ctx: ::typeck::Context<'src>
}

impl<'src> InferPass<'src> {
    pub fn new(ctx: ::typeck::Context<'src>) -> Self {
        InferPass {
            ctx: ctx
        }
    }    
}

impl<'src> InferPass<'src> {
    fn infer(&mut self, lhs: ast::NodeId, rhs: ast::NodeId, ty_id: ast::TyId) {
        self.ctx.insert_node_ty(lhs, ty_id);
        self.ctx.insert_node_ty(rhs, ty_id);   
    }
    
    fn infer_decl(&mut self, decl: &ast::Decl, node_id: ast::NodeId) {
        match decl {
            &ast::Decl::Local(box ref local) => {
                let expr = local.expr();
                match expr.kind() {
                    &ast::ExprKind::Unit => {
                        self.infer(local.id(), expr.id(), ast::TyId(4));                              
                    }
                    &ast::ExprKind::Number(_) => {
                        self.infer(local.id(), expr.id(), ast::TyId(1));  
                    },
                    &ast::ExprKind::StrLit(_) => {
                        self.infer(local.id(), expr.id(), ast::TyId(2));  
                    },
                    &ast::ExprKind::Bool(_) => {
                        self.infer(local.id(), expr.id(), ast::TyId(3));  
                    },
                    &ast::ExprKind::Call(name, ref args) => {
                        let fn_id = self.ctx.resolve_ident(name).unwrap();
                        let fn_ty = self.ctx.resolve_node_ty(fn_id).unwrap();
                                
                        self.infer(local.id(), expr.id(), fn_ty); 
                    },
                    &ast::ExprKind::Value(val) => {
                        // Look up the variable in our ident table to get the node id
                        let val_id = self.ctx.resolve_ident(val).expect("val_id empty");
                                
                        // Use the node id to look up the ty id of the node
                        let val_ty = self.ctx.resolve_node_ty(val_id).expect("val_ty empty");
                                
                        // Map both the declaration and expression nodes to the resolved ty id
                        self.infer(local.id(), expr.id(), val_ty); 
                    },
                    _ => {}
                }
            },
            _ => {}
        }         
    }
    
    fn infer_assign(&mut self, name: ast::Name, expr: &ast::Expr, node_id: ast::NodeId) {
        let ident_id = self.ctx.resolve_ident(name).expect("bfailed to resolve ident to node id for assignment");
        let ident_ty = self.ctx.resolve_node_ty(ident_id).expect("failed to resolve ident to ty id for assignment");
        
        match expr.kind() {
            &ast::ExprKind::Value(ident) => {
                let val_id = self.ctx.resolve_ident(ident).expect("failed to resolve ident to node id for assignment");
                // This <id> should already be present.
                let val_ty = self.ctx.resolve_node_ty(val_id).expect("failed to resolve ident to ty id for assigment");            
                       
                self.ctx.insert_node_ty(node_id, ident_ty);
                self.ctx.insert_node_ty(expr.id(), val_ty);
            },
            &ast::ExprKind::StrLit(lit) => {
                self.ctx.insert_node_ty(node_id, ident_ty);
                self.ctx.insert_node_ty(expr.id(), ast::TyId(2));                         
            },
            &ast::ExprKind::Number(num) => {
                self.ctx.insert_node_ty(node_id, ident_ty);
                self.ctx.insert_node_ty(expr.id(), ast::TyId(1));                         
            },
            _ => {}
        }
    }
    
    fn infer_expr(&mut self, expr: &ast::Expr, node_id: ast::NodeId) {
        match expr.kind() {
            &ast::ExprKind::Value(ident) => {
                let val_id = self.ctx.resolve_ident(ident).expect("failed to resolve ident to node id for assignment");
                // This <id> should already be present.
                let val_ty = self.ctx.resolve_node_ty(val_id).expect("failed to resolve ident to ty id for assigment");            
                       
                self.ctx.insert_node_ty(expr.id(), val_ty);
            },
            &ast::ExprKind::Bool(_) => {
                self.ctx.insert_node_ty(expr.id(), ast::TyId(3));  
            }
            &ast::ExprKind::StrLit(lit) => {
                self.ctx.insert_node_ty(expr.id(), ast::TyId(2));                         
            },
            &ast::ExprKind::Number(num) => {
                self.ctx.insert_node_ty(expr.id(), ast::TyId(1));                         
            },
            _ => {}
        }        
    }
}

impl<'src> ast::Visitor for InferPass<'src> {
    type Output = ::typeck::Context<'src>;

    fn finalize(self) -> Self::Output {
        self.ctx
    }
    
    fn visit_block(&mut self, block: &ast::Block) {
        self.ctx.push_scope(block.id());
        ast::walk_block(self, block);
        self.ctx.pop_scope();
    }
    
    fn visit_statement(&mut self, stmt: &ast::Statement) {
        match stmt {
            &ast::Statement::Decl(box ref decl, node_id) => {
                self.infer_decl(decl, node_id);       
            },
            &ast::Statement::Assign(name, box ref expr, node_id) => {
                self.infer_assign(name, expr, node_id);
            },
            &ast::Statement::Block(box ref block, _) => {
                self.visit_block(block)
            },
            &ast::Statement::If(box ref expr, box ref block, node_id) => {
                self.infer_expr(expr, node_id);
                self.visit_block(block);
            },
            _ => {}
        }
     }    
}
