use ast::*;

#[derive(Debug, Clone)]
pub struct Local {
    id: NodeId,
    name: Name,
    expr: Box<Expr>
}

impl Local {
    pub fn new(id: NodeId, name: Name, expr: Expr) -> Self {
        Local {
            id: id,
            name: name,
            expr: Box::new(expr)
        }
    }

    pub fn id(&self) -> NodeId {
        self.id
    } 
    
    pub fn name(&self) -> Name {
        self.name
    }
     
    pub fn expr(&self) -> &Expr {
        &self.expr
    }
}