use ast::*;

#[derive(Debug, Clone)]
pub struct Block {
    id: NodeId,
    statements: Vec<Statement>
}

impl Block {
    pub fn new(id: NodeId) -> Self {
        Block {
            id: id,
            statements: vec![]
        }
    }
    
    pub fn id(&self) -> NodeId {
        self.id
    }
    
    pub fn statements(&self) -> &[Statement] {
        &self.statements
    }
    
    pub fn insert_statement(&mut self, statement: Statement) {
        self.statements.push(statement);
    }
}