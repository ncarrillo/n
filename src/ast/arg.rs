use ast::*;

#[derive(Clone, Hash, Debug)]
pub struct Arg {
    id: NodeId,
    expr: Box<Expr>
}

impl Arg {
    pub fn new(id: NodeId, expr: Expr) -> Self {
        Arg {
            id: id,
            expr: Box::new(expr)
        }
    }
    
    pub fn id(&self) -> NodeId {
        self.id
    }
    
    pub fn expr(&self) -> &Expr {
        &self.expr
    }
}