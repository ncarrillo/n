use ast::*;

#[derive(Debug, Clone)]
pub enum Statement {
    Decl(Box<Decl>, NodeId),
    Assign(Name, Box<Expr>, NodeId),
    Block(Box<Block>, NodeId),
    Expr(Box<Expr>, NodeId),
    If(Box<Expr>, Box<Block>, NodeId)
}

impl Statement {
    pub fn new_decl(id: NodeId, decl: Decl) -> Self {
        Statement::Decl(
            Box::new(decl),
            id
        )
    }

    pub fn new_if(id: NodeId, expr: Expr, block: Block) -> Self {
        Statement::If(
            Box::new(expr),
            Box::new(block),
            id
        )
    }
    
    pub fn new_expr(id: NodeId, expr: Expr) -> Self {
        Statement::Expr(
            Box::new(expr),
            id
        )
    }
    
    pub fn new_block(id: NodeId, block: Block) -> Self {
        Statement::Block(
            Box::new(block),
            id
        )
    }
        
    pub fn new_assign(id: NodeId, name: Name, expr: Expr) -> Self {
        Statement::Assign(
            name,
            Box::new(expr),
            id
        )
    }
}
