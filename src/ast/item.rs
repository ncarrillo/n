use ast::*;

#[derive(Debug, Clone)]
pub struct Item {
    id: NodeId,
    name: Name,
    kind: ItemKind,
}

impl Item {
    pub fn new(id: NodeId, name: Name, kind: ItemKind) -> Self {
        Item {
            id: id,
            name: name,
            kind: kind
        }
    }
    
    pub fn id(&self) -> NodeId {
        self.id
    }
    
    pub fn name(&self) -> Name {
        self.name
    }
    
    pub fn kind(&self) -> &ItemKind {
        &self.kind
    }
}

#[derive(Debug, Clone)]
pub enum ItemKind {
    Use,
    Const,
    Module(Box<Module>),
    Fn(Box<FnDecl>, Box<Block>)
}

impl ItemKind {
    pub fn new_fn(decl: FnDecl, block: Block) -> ItemKind {
        ItemKind::Fn(
            Box::new(decl),
            Box::new(block)
        )
    }
    
    pub fn new_module(module: Module) -> ItemKind {
        ItemKind::Module(Box::new(module))
    }
}