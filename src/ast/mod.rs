mod arg;
mod block;
mod decl;
mod expr;
mod fn_decl;
mod item;
mod krate;
mod local;
mod module;
mod statement;
mod visitor;

pub use ast::arg::*;
pub use ast::block::*;
pub use ast::decl::*;
pub use ast::expr::*;
pub use ast::fn_decl::*;
pub use ast::item::*;
pub use ast::krate::*;
pub use ast::local::*;
pub use ast::module::*;
pub use ast::statement::*;
pub use ast::visitor::*;

use std::fmt;
use std::hash::{Hash, Hasher, SipHasher};

pub type Span = (usize, usize, usize);

#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct Name(pub u64);

#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct NodeId(pub u64);

#[derive(Copy, Clone, Hash, Eq, PartialEq)]
pub struct TyId(pub u64);

pub fn hash<T: Hash>(t: &T) -> u64 {
    let mut s = SipHasher::new();
    t.hash(&mut s);
    s.finish()
}
    
impl TyId {
    pub fn from_fn(fn_decl: &FnDecl) -> TyId  {
        TyId(hash(fn_decl))        
    }
}

impl fmt::Debug for Name {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl fmt::Debug for NodeId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl fmt::Debug for TyId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}