use ast::*;

#[derive(Clone, Hash, Debug)]
pub struct Expr {
    id: NodeId,
    kind: ExprKind
}

impl Expr {
    pub fn new(id: NodeId, kind: ExprKind) -> Self {
        Expr {
            id: id,
            kind: kind
        }
    }
    
    pub fn id(&self) -> NodeId {
        self.id
    }
    
    pub fn kind(&self) -> &ExprKind {
        &self.kind
    }
}

#[derive(Clone, Hash, Debug)]
pub enum ExprKind {
    Unit,
    Call(Name, Vec<Arg>),
    Number(usize),
    StrLit(Name),
    Value(Name), 
    Bool(bool),
    Add(Box<Expr>, Box<Expr>),
    Sub(Box<Expr>, Box<Expr>),
    Cmp(Box<Expr>, NodeId)
}