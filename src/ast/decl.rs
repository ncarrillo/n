use ast::*;

#[derive(Debug, Clone)]
pub enum Decl {
    Local(Box<Local>),
    Item(Box<Item>)
}

impl Decl {
    pub fn new_local(local: Local) -> Self {
        Decl::Local(
            Box::new(local)
        )
    }
}