use ast::*;

#[derive(Debug, Clone)]
pub struct Module {
    id: NodeId,
    items: Vec<Item>
}

impl Module {
    pub fn new(id: NodeId, items: Vec<Item>) -> Self {
        Module {
            id: id,
            items: items
        }
    }
    
    pub fn id(&self) -> NodeId {
        self.id
    }
    
    pub fn items(&self) -> &[Item] {
        &self.items
    }
}
