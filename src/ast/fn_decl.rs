use ast::*;

#[derive(Clone, Hash, Debug)]
pub struct FnDecl;

impl FnDecl {
    pub fn new() -> Self {
        FnDecl
    }
}