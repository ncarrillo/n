use ast::*;

#[derive(Debug)]
pub struct Krate {
    name: Name,
    module: Module
}

impl Krate {
    pub fn new(name: Name, module: Module) -> Self {
        Krate {
            name: name,
            module: module
        }
    }
    
    pub fn name(&self) -> Name {
        self.name
    }
    
    pub fn module(&self) -> &Module {
        &self.module
    }
}
