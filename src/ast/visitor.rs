use ::ast;

macro_rules! walk_list {
    ($v:expr, $list:expr, $action:ident, $walker:ident) => {
        for item in $list {
            $v.$action(item);
            $walker($v, item);
        }
    };
    
    ($v:expr, $list:expr, $action:ident) => {
        for item in $list {
            $v.$action(item);
        }
    }    
}

pub fn walk_krate<V: Visitor>(visitor: &mut V, krate: &ast::Krate) {
    visitor.visit_module(krate.module());
}

pub fn walk_item<V: Visitor>(visitor: &mut V, item: &ast::Item) {
    match item.kind() {
        &ast::ItemKind::Fn(ref decl, ref block) => {
            visitor.visit_block(&block);
        },
        &ast::ItemKind::Module(ref module) => {
            visitor.visit_module(&module);
        },
        _ => {}
    }
}

pub fn walk_module<V: Visitor>(visitor: &mut V, module: &ast::Module) {
    walk_list!(visitor, module.items(), visit_item);    
}

pub fn walk_block<V: Visitor>(visitor: &mut V, block: &ast::Block) {
    walk_list!(visitor, block.statements(), visit_statement);    
}

pub trait Visitor where Self: Sized {
    type Output;
    
    fn finalize(self) -> Self::Output;
  
    fn visit_item(&mut self, item: &ast::Item) {
        walk_item(self, item);
    }

    fn visit_block(&mut self, block: &ast::Block) {
        walk_block(self, block);
    }
    
    fn visit_statement(&mut self, item: &ast::Statement) {}
    
    fn visit_module(&mut self, module: &ast::Module) {
        walk_module(self, module);
    }
    
    fn visit_krate(&mut self, krate: &ast::Krate) {}
}